/* tslint:disable:no-switch-case-fall-through */
import React from 'react';
import ReactDOM from 'react-dom';
import Modal from './Modal';
import closest from '../_util/closest';
export default function prompt(title, message, callbackOrActions) {
    var type = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'default';
    var defaultValue = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : '';
    var placeholders = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : ['', ''];
    var platform = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : 'ios';

    if (!callbackOrActions) {
        // console.log('Must specify callbackOrActions');
        return {
            close: function close() {}
        };
    }
    var prefixCls = 'am-modal';
    var data = {};
    function onChange(e) {
        var target = e.target;
        var inputType = target.getAttribute('type');
        data[inputType] = target.value;
    }
    var inputDom = void 0;
    var focusFn = function focusFn(input) {
        setTimeout(function () {
            if (input) {
                input.focus();
            }
        }, 500);
    };
    switch (type) {
        case 'login-password':
            inputDom = React.createElement(
                'div',
                { className: prefixCls + '-input-container' },
                React.createElement(
                    'div',
                    { className: prefixCls + '-input' },
                    React.createElement(
                        'label',
                        null,
                        React.createElement('input', { type: 'text', value: data.text, defaultValue: defaultValue, ref: function ref(input) {
                                return focusFn(input);
                            }, onChange: onChange, placeholder: placeholders[0] })
                    )
                ),
                React.createElement(
                    'div',
                    { className: prefixCls + '-input' },
                    React.createElement(
                        'label',
                        null,
                        React.createElement('input', { type: 'password', value: data.password, defaultValue: '', onChange: onChange, placeholder: placeholders[1] })
                    )
                )
            );
            break;
        case 'secure-text':
            inputDom = React.createElement(
                'div',
                { className: prefixCls + '-input-container' },
                React.createElement(
                    'div',
                    { className: prefixCls + '-input' },
                    React.createElement(
                        'label',
                        null,
                        React.createElement('input', { type: 'password', value: data.password, defaultValue: '', ref: function ref(input) {
                                return focusFn(input);
                            }, onChange: onChange, placeholder: placeholders[0] })
                    )
                )
            );
            break;
        case 'plain-text':
        case 'default':
        default:
            inputDom = React.createElement(
                'div',
                { className: prefixCls + '-input-container' },
                React.createElement(
                    'div',
                    { className: prefixCls + '-input' },
                    React.createElement(
                        'label',
                        null,
                        React.createElement('input', { type: 'text', value: data.text, defaultValue: defaultValue, ref: function ref(input) {
                                return focusFn(input);
                            }, onChange: onChange, placeholder: placeholders[0] })
                    )
                )
            );
            break;
    }
    var content = React.createElement(
        'div',
        null,
        message,
        inputDom
    );
    var div = document.createElement('div');
    document.body.appendChild(div);
    function close() {
        ReactDOM.unmountComponentAtNode(div);
        if (div && div.parentNode) {
            div.parentNode.removeChild(div);
        }
    }
    function getArgs(func) {
        var text = data.text || defaultValue || '';
        var password = data.password || '';
        if (type === 'login-password') {
            return func(text, password);
        } else if (type === 'secure-text') {
            return func(password || defaultValue);
        }
        return func(text);
    }
    var actions = void 0;
    if (typeof callbackOrActions === 'function') {
        actions = [{ text: '取消' }, { text: '确定', onPress: function onPress() {
                getArgs(callbackOrActions);
            } }];
    } else {
        actions = callbackOrActions.map(function (item) {
            return {
                text: item.text,
                onPress: function onPress() {
                    if (item.onPress) {
                        return getArgs(item.onPress);
                    }
                }
            };
        });
    }
    var footer = actions.map(function (button) {
        var orginPress = button.onPress || function () {};
        button.onPress = function () {
            var res = orginPress();
            if (res && res.then) {
                res.then(function () {
                    close();
                });
            } else {
                close();
            }
        };
        return button;
    });
    function onWrapTouchStart(e) {
        // exclude input element for focus
        if (!/iPhone|iPod|iPad/i.test(navigator.userAgent)) {
            return;
        }
        var pNode = closest(e.target, '.' + prefixCls + '-content');
        if (!pNode) {
            e.preventDefault();
        }
    }
    ReactDOM.render(React.createElement(
        Modal,
        { visible: true, transparent: true, prefixCls: prefixCls, title: title, closable: false, maskClosable: false, transitionName: 'am-zoom', footer: footer, maskTransitionName: 'am-fade', platform: platform, wrapProps: { onTouchStart: onWrapTouchStart } },
        React.createElement(
            'div',
            { className: prefixCls + '-propmt-content' },
            content
        )
    ), div);
    return {
        close: close
    };
}