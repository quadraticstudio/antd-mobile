/// <reference types="react" />
import * as React from 'react';
import tsPropsType from './PropsType';
export interface PickerPropsType extends tsPropsType {
    pickerPrefixCls?: string;
    popupPrefixCls?: string;
}
export declare function getDefaultProps(): {
    triggerType: string;
    prefixCls: string;
    pickerPrefixCls: string;
    popupPrefixCls: string;
    format: (values: any) => any;
    cols: number;
    cascade: boolean;
    title: string;
};
export default abstract class AbstractPicker extends React.Component<PickerPropsType, any> {
    protected abstract popupProps: {};
    private scrollValue;
    getSel: () => void | undefined;
    getPickerCol: () => any;
    onOk: (v: any) => void;
    setScrollValue: (v: any) => void;
    setCasecadeScrollValue: (v: any) => void;
    fixOnOk: (cascader: any) => void;
    onPickerChange: (v: any) => void;
    render(): JSX.Element;
}
