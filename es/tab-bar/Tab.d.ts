/// <reference types="react" />
import * as React from 'react';
declare class Tab extends React.PureComponent<any, any> {
    renderIcon: () => JSX.Element;
    onClick: () => void;
    render(): JSX.Element;
}
export default Tab;
