/// <reference types="react" />
import * as React from 'react';
export default class DrawerExample extends React.Component<any, any> {
    drawer: any;
    onOpenChange: (isOpen: any) => void;
    render(): JSX.Element;
}
